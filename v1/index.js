//Dependencies
let express = require("express");
let app = require("express")();
let http = require("http").Server(app);
let credentials = require("./config.js");
let port = 3002;
var path = require("path");

//Middleware Dependencies
let bodyParser = require("body-parser");
let cookieParser = require("cookie-parser");

//Express Config
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.static("dist"));

//Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

// app.use(express.static(__dirname + "/dist"));

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/dist/index.html"));
});

//Routes
require("./app/routes/index.js")(app);

//Initiate
http.listen(port, function() {
  console.log("listening on *:3002");
});
