import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

//VTooltip
import VTooltip from "v-tooltip";
Vue.use(VTooltip);

//VModal
import VModal from "vue-js-modal";
Vue.use(VModal);

new Vue({
  router,
  store,
  render: h => h(App),
  mounted() {
    // Prevent blank screen in Electron builds
    this.$router.push("/");
  }
}).$mount("#app");
