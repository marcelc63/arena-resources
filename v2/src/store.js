import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import create from "@/store/create.js";

export default new Vuex.Store({
  modules: {
    create: create
  }
});