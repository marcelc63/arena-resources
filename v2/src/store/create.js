import _ from "lodash";

function skill() {
  return {
    name: "",
    picture: "",
    description: "",
    cooldown: 0,
    energy: {
      a: 0,
      b: 0,
      c: 0,
      d: 0,
      e: 0,
      amount: ""
    },
    classes: ""
  };
}

export default {
  namespaced: true,
  state: {
    zenMode: false,
    renderKey: 0,
    data: {
      profile: {
        name: "",
        picture: "",
        pictures: "",
        author: "",
        description: ""
      },
      skills: [
        _.cloneDeep(skill()),
        _.cloneDeep(skill()),
        _.cloneDeep(skill()),
        _.cloneDeep(skill())
      ]
    },
    theme: {
      border: true,
      classes: true,
      background: "#fff",
      font: "",
      fontColor: "",
      contentColor: "",
      energy: {
        color: {
          a: "#11DD2D",
          b: "#F0000A",
          c: "#01A9D5",
          d: "#ffffff",
          e: "#222222"
        },
        type: "color" //Colors or Number
      }
    },
    preset: {
      label: {
        author: "Author",
        pictures: "Pictures",
        skill: "Skill",
        energy: "Energy",
        cooldown: "Cooldown",
        classes: "Classes"
      },
      classes: [
        "Instant",
        "Action",
        "Control",
        "Melee",
        "Ranged",
        "Mental",
        "Energy",
        "Strategic"
      ]
    },
    upload: {
      type: null,
      id: null
    }
  },
  mutations: {
    skill: (state, payload) => {
      let { index, value, component } = payload;

      if (component === "picture") {
        state.data.skills[index].picture = value;
        return;
      }

      if (component === "cooldown") {
        console.log("hi");
        state.data.skills[index].cooldown = value;
        return;
      }

      if (component === "classes") {
        state.data.skills[index].classes = value;
        return;
      }

      if (component === "description") {
        state.data.skills[index].description = value;
        return;
      }

      if (component === "energy") {
        let { energyType } = payload;
        let temp = state.data.skills[index].energy;
        temp[energyType] = value;
        state.data.skills[index].energy = {
          ...temp
        };
        return;
      }

      let temp = state.data.skills[index];
      temp[component] = value;

      state.data.skills[index] = {
        ...temp
      };
    },
    skillRemove: (state, payload) => {
      let { index } = payload;
      state.data.skills = state.data.skills.filter((x, i) => i !== index);
    },
    skillAdd: (state, payload) => {
      let { index } = payload;
      state.data.skills = state.data.skills.concat(_.cloneDeep(skill()));
    },
    profile: (state, payload) => {
      let { value, component } = payload;
      let temp = state.data.profile;
      temp[component] = value;

      state.data.profile = {
        ...temp
      };
    },
    label: (state, payload) => {
      let { value, component } = payload;
      let temp = state.preset.label;
      temp[component] = value;

      state.preset.label = {
        ...temp
      };
    },
    energy: (state, payload) => {
      let { value, component } = payload;
      let temp = state.theme.energy.color;
      temp[component] = value;

      state.theme.energy.color = {
        ...temp
      };
    },
    energyType: (state, payload) => {
      let { value } = payload;
      state.theme.energy.type = value;
    },
    classes: (state, payload) => {
      let { to, value } = payload;
      if (to === "add") {
        state.preset.classes = state.preset.classes.concat(value);
      } else if (to === "remove") {
        state.preset.classes = state.preset.classes.filter(
          (x, i) => i !== value
        );
      }
    },
    theme: (state, payload) => {
      let { value, component } = payload;
      let temp = state.theme;
      temp[component] = value;

      state.theme = {
        ...temp
      };
    },
    load: (state, payload) => {
      let { data, theme, preset } = payload;

      state.data = data;
      state.theme = theme;
      state.preset = preset;
    },
    renderKey: state => {
      state.renderKey = state.renderKey + 1;
    }
  },
  actions: {
    skill: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "change") {
        commit("skill", payload);
      } else if (to === "remove") {
        commit("skillRemove", payload);
      } else if (to === "add") {
        commit("skillAdd", payload);
      }
    },
    profile: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "change") {
        commit("profile", payload);
      }
    },
    theme: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "change") {
        commit("theme", payload);
      }
    },
    label: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "change") {
        commit("label", payload);
      }
    },
    energy: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "change") {
        commit("energy", payload);
      } else if (to === "type") {
        commit("energyType", payload);
      }
    },
    classes: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "add") {
        commit("classes", payload);
      } else if (to === "remove") {
        commit("classes", payload);
      }
    },
    load: ({ commit }, payload) => {
      commit("load", payload);
    },
    renderKey: ({ commit }) => {
      commit("renderKey");
    }
  },
  getters: {
    skills: function(state) {
      return state.data.skills;
    },
    theme: function(state) {
      return state.theme;
    },
    preset: function(state) {
      return state.preset;
    },
    label: function(state) {
      return state.preset.label;
    },
    classes: function(state) {
      return state.preset.classes;
    },
    renderKey: function(state) {
      return state.renderKey;
    }
  }
};
